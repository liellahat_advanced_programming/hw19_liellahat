﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex1
{
    public partial class CalendarForm : Form
    {
        private string username;
        private List<Birthday> bdList;

        // this function is called when the calendar form is opened, it reads the data in the birthday file and stores it in a list. 
        public CalendarForm(string username)
        {
            this.username = username;
            this.bdList = new List<Birthday>();
            var bdFile = System.IO.File.ReadAllText(@"D:\Users\user-pc\Desktop\HW19_liellahat\ex1\" + this.username + "BD.txt").Split('\r');

            foreach (var line in bdFile)
            {
                var s = line[0] == '\n' ? line.Remove(0, 1).Split(',') : line.Split(',');  // s contains the username and the birthday.
                this.bdList.Add(new Birthday(s[0], s[1])); 
            }

            InitializeComponent();
            label1.Text = "hello";   
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CalendarForm_Load(object sender, EventArgs e)
        {

        }

        // this function is called when the user presses a date in the calendar.
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            List<int> bdParts;

            for (var i = 0; i < this.bdList.Count; i++)  // checking if the date exists in the birthdays list.
            {
                bdParts = bdList[i].GetDateParts();

                if (bdParts[0] == e.Start.Month && bdParts[1] == e.Start.Day)
                {
                    label1.Text = "it's " + bdList[i].name + "'s birthday today";  
                    return;
                }
            }

            label1.Text = "no one is having his birthday today";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
