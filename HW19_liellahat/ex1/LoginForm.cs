﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex1
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        // sign in button.  
        private void button1_Click(object sender, EventArgs e)
        {
            var username = textBox1.Text;  
            var password = textBox2.Text;
            var details = System.IO.File.ReadAllText(@"D:\Users\user-pc\Desktop\HW19_liellahat\ex1\Users.txt").Split('\r');
        
            foreach (var line in details) 
            {
                var s = line[0] == '\n' ? line.Remove(0, 1).Split(',') : line.Split(',');  // s contains the username and the password.

                if (s[0] == username && s[1] == password)
                {
                    CalendarForm cForm = new CalendarForm(username);  
                    this.Hide();
                    cForm.ShowDialog();
                    return;
                }
            }

            MessageBox.Show("Username does not match password.");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
