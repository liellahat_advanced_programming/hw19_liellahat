﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex1
{
    public class Birthday
    {
        public string name { get; set; }
        public string birthday {get; set;}

        public Birthday(string name, string birthday)
        {
            this.name = name;
            this.birthday = birthday;
        }

        public List<int> GetDateParts()
        {
            List<int> dateParts = new List<int>();
            dateParts.Add(int.Parse(this.birthday.Split('/')[0]));
            dateParts.Add(int.Parse(this.birthday.Split('/')[1]));

            return dateParts;
        }
    }
}
